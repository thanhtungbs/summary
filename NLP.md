# NLP
> The key ideas is __word embeddings__ that is a way of representing words.

## Word representation
---
> Vocabulary:
    
` V = [a, aaron,.., zulu, <UNK>]`

`|V|` = 10.000

1. One hot representation
2. Word embedding

We could learn a set of features and values for each of them 