# Deep learning have two major model:
- CNN for image input 
- RNN for sequence input

# __Recurrent neural networks(RNN)__
1. What is RNNs?
2. Advantage and disadvantages of RRNs.
3. The problem
4. Application of RNNs
5. Solution
6. Review
## 1. __What is RNNs?__
> Human do not start their thinking from scratch evert second. As you read something, you can understand each word based on your understanding of <u>previous</u> words.

> Traditional neural networks can not do this. It's unclear how a tradional nets could use its reasoning about previous events in the film to inform later ones.

> __Recurrent neural networks__ address this issue. They are networks with loops in them, allowing information to persist.

<div style="text-align:center">
    <img src="images/RNN-rolled.png" alt="RNN roll" width="100px"/>
    <div>Recurrent Neural Networks have loops</div>
</div>

<div style="text-align:center">
    <img src="images/processing_sequence.gif" />
    <div>Processing sequence one by one</div>
</div>

### 1.1 __Tanh activation__
> The tanh activation is used to help regulate the values flowing through the network. The tanh function squishes values to always be between -1 and 1.
<div style="text-align:center">
    <img src="images/tanh.gif" />
    <div>Tanh squishes values to be between -1 and 1</div>
</div>

> When vectors are flowing through a neural network, it undergoes many transformations due to various math operations. So imagine a value that continues to be multiplied by let’s say 3. You can see how some values can __explode__ and become astronomical, causing other values to seem insignificant.
<div style="text-align:center">
    <img src="images/no_tanh.gif" />
    <div>vector transformations without tanh</div>
</div>

> That why __a tanh function__ ensures that the values stay between -1 and 1, thus regulating the output of the neural network. You can see how the same values from above remain between the boundaries allowed by the tanh function.
<div style="text-align:center">
    <img src="images/have_tanh.gif" />
    <div>vector transformations without tanh</div>
</div>

## 2. __Advantage and disadvantages of RRNs.__
> The major disadvantage of RNNs are the vanishing gradient and gradient exploding problem. It makes the training of RNN difficult in several ways.

1. It cannot process very long sequences if it uses tanh as its activation function
2. It is very unstable if we use ReLu as its activation function
3. RNNs cannot be stacked into very deep models
4. RNNs are not able to keep track of long-term dependencies

## **Solution**: to learn very long range connection in a sequence -> the other type of unit that allows you to do
- Gated Recurrent Unit (GRU)
- Long short term memory (LSTM) (*more powerful than the GRU, more general version of the GRU*)

--> __Goal__: to capture much longer range dependencies.

## 3. __The problem__
> RNNs suffer from short-term-memory that means if a sequence is long enough, they'll have a hard time carrying information from earlier time steps to later ones. So if you are trying to process a paragraph of text to do predictions --> RNN's may leave out important information from the beginning. 

---
> During __back propagation__, RNNs suffer from the __vanishing gradient problem__. 
- __Vanishing Gradient__ problem mostly occurs when we are dealing with large time series data sets.
- Gradients are values used to update a neural networks weights. The vanishing gradient problem is when the gradient shrinks as it back propagates through time. If a gradient value becomes extremely small, <u>it doesn’t contribute too much learning</u>.
- That why RNN’s can forget what it seen in longer sequences, thus having a short-term memory. 
![](images/vanishing_gra.png)

---

- Similarly there is issues of increasing gradients at each step called as __Exploding gradients__


## 4. __Application of RNNs__
> LSTM’s and GRU’s can be found in speech recognition, speech synthesis, and text generation. You can even use them to generate captions for videos.
- Speech to text.
- Sentiment classification: phân loại số sao cho các bình luận, ví dụ: input: “ứng dụng tốt”, output: 4 sao.
- Machine translation.
- Video recognition: Action detection.
- Heart attack: Dự đoán đột quỵ tim.

## 5. __Solution__
### 5.1. __LSTM's and GRU's__
- It were created as the solution to short-term-memory.

<div style="text-align:center">
    <img src="images/lstm_gru.png" />
</div>

- These gates can learn which data in a sequence is important to keep or throw away.

- By doing that, it can pass relevant information down the long chain of sequences to make predictions.

### 5.1.1 __Gated Recurrent Unit (GRU)__
> Two gates : the optic gate and the relevance gate

<div style="text-align:center">
    <img src="images/gru_cell.png" width="450px"/>
    <img src="images/update_gate_gru.png" width="450px"/>
    <div>GRU cell and it’s gates</div>
</div>

### 5.1.2 __Long short term memory (LSTM)__
#### *Core Concept
> The core concept of LSTM’s are the cell state, and it’s various gates. 

> You can think of it as the “memory” of the network. The cell state, in theory, can carry relevant information throughout the processing of the sequence. So even information from the earlier time steps can make it’s way to later time steps, reducing the effects of short-term memory.

#### *Sigmoid
> Gates contains sigmoid activations. A sigmoid activation is similar to the tanh activation. Instead of squishing values between -1 and 1, it squishes values between 0 and 1.
- That is helpful to update or forget data because any number getting multiplied by 0 is 0, causing values to disappears or be “forgotten.” 
- Any number multiplied by 1 is the same value therefore that value stay’s the same or is “kept.” 
  
<div style="text-align:center">
    <img src="images/sigmoid.gif" />
    <div>Sigmoid squishes values to be between 0 and 1</div>
</div>

#### *Three gate: 
- __The forget gate__: This gate decides what information should be thrown away or kept
  - The current input is passed through __the sigmoid function__. 
  - Values come out between 0 and 1. The closer to 0 means to forget, and the closer to 1 means to keep.
    <div style="text-align:center">
        <img src="images/forget_gate_lstm.gif" />
        <div>Forget gate operations</div>
    </div>

- __The input gate__: to update the cell state, we have the input gate.
  - First, we pass the previous hidden state and current input into a sigmoid function. That decides which values will be updated by transforming the values to be between 0 and 1.
  - 0 means not important, and 1 means important.
  - You also pass the hidden state and current input into the tanh function to squish values between -1 and 1 to help regulate the network.
  -  Then you multiply the tanh output with the sigmoid output. The sigmoid output will decide which information is important to keep from the tanh output.
    <div style="text-align:center">
        <img src="images/input_gate_lstm.gif" />
        <div>Input gate operations</div>
    </div>
- __The output gate__ when i have enough information to calculate the cell state. The output gate decides what the next hidden state should be. The hidden state is also used for predictions.
  - First, we pass the previous hidden state and the current input into a sigmoid function.
  - we pass the newly modified cell state to the tanh function.
  - We multiply the tanh output with the sigmoid output to decide what information the hidden state should carry. 
  - The output is the hidden state. 
    <div style="text-align:center">
        <img src="images/output_gate_lstm.gif" />
        <div>Output gate operations</div>
    </div>

> cell state:
![](images/LSTM_cell.png)

> Formular:
![](images/lstm_formular.png)


## 6. __Review__
> __The advantage of the GRU__ :
- It's __a simpler model__ and so it is actually __easier to build__ a much bigger network, it only has two gates, so computationally, it runs a bit __faster__.
- They're a bit simpler but often work just as well. 

> __LSTM__
- more powerful and more effective since it has three gates instead of two. 
- If you want to pick one to use, LSTM has been the historically more proven choice. So, if you had to pick one
- I think most people today will still use the LSTM as the default first thing to try.

---> **Goal** : to capture much longer range dependencies.


## __Bidirectional RNNs (BRNN)__

Example : in the context of named entity recognition
"He said, "Teddy bears are on sale!"

> Making this change you can have a model that uses RNN and or GRU or LSTM and is able to make predictions anywhere even in the middle of a sequence by taking into account information potentially from the entire sequence. 

> __The disadvantage of the bidirectional RNN__ is that you do need the entire sequence of data before you can make predictions anywhere


---> __a speech recognition prediction__.